module.exports = {
  plugins: {
    //   vuecli已经配置过所以这里注释掉
    // autoprefixer: {
    //   browsers: ['Android >= 4.0', 'iOS >= 8']
    // },
    'postcss-pxtorem': {
      // lib-flexible 的rem适配方案：把一行分为10份 每份就是十分之一  故这里的rootValue就是设计稿宽度的十分之一
      // vant建议设计为37.5  因为vant是基于375去写的 所以必须设置为37.5 我们的设计稿的750 故缺点是使用我们设计稿的尺寸必须每次都除以2
      // 如果是vant的样式 使用37.5 我们自己的样式 使用75来转换
      // rootValue 支持数字和函数两种配置方式 故这里进行修改 一种返回固定值 一种根据postcss-pxtorem处理每个css文件的时候都会调用这个函数 它会把被处理的css文件相关信息通过参数传递给该函数
      rootValue({ file }) {
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      },
      // 配置要转换的属性 *表示所有
      propList: ['*'],
      // 配置不要转换的样式资源
      exclude: 'github-markdown'
    }
  }
}
