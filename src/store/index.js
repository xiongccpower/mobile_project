import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem } from '@/utils/storage.js'

Vue.use(Vuex)

const tokenString = 'tokenString'
export default new Vuex.Store({
  state: {
    user: getItem(tokenString),
    userStatus: true
  },
  mutations: {
    setUser(state, data) {
      state.user = data
      setItem(tokenString, data)
    }
  },
  actions: {},
  modules: {}
})
