import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入vant及其样式文件
import Vant from 'vant'
import 'vant/lib/index.css'
// 引入全局样式表
import './style/index.less'
// 动态设置rem值
import 'amfe-flexible'
// 引入dayjs
import './utils/dayjs'

// 注册vant
Vue.use(Vant)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
