// 封装本地存储token

// 存储token
export const setItem = (storageName, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  return window.sessionStorage.setItem(storageName, value)
}

// 获取token
export const getItem = storageName => {
  try {
    return JSON.parse(window.sessionStorage.getItem(storageName))
  } catch (err) {
    return window.sessionStorage.getItem(storageName)
  }
}

// 删除token
export const removeItem = storageName => {
  return window.sessionStorage.removeItem(storageName)
}
