import request from '@/utils/request.js'

// 获取文章评论请求模块
export const getArticleComments = params => {
  return request({
    method: 'GET',
    url: '/app/v1_0/comments',
    // Get 参数使用params进行传递
    // 我们写的时候是对象 但是最终发给后端的数据是？
    // axios会把params对象转为key=value?key=value的格式放到url中
    params
  })
}

// 评论点赞
export const addCommentLike = target => {
  return request({
    method: 'POST',
    url: '/app/v1_0/comment/likings',
    data: {
      target
    }
  })
}

// 取消评论点赞
export const cancelCommentLike = target => {
  return request({
    method: 'DELETE',
    url: `/app/v1_0/comment/likings/${target}`
  })
}

// 发布评论
export const applyComments = data => {
  return request({
    method: 'POST',
    url: '/app/v1_0/comments',
    data
  })
}
