// 文章列表数据请求模块
import request from '@/utils/request'

// 获取文章列表数据请求模块
export const getArticleList = params => {
  return request({
    methods: 'GET',
    url: '/app/v1_1/articles',
    params
  })
}

// 获取文章详情请求模块
export const getArticleInfos = articleId => {
  return request({
    method: 'GET',
    url: `/app/v1_0/articles/${articleId}`
  })
}

// 收藏文章接口模块
export const collectArticles = target => {
  return request({
    method: 'POST',
    url: '/app/v1_0/article/collections',
    data: {
      target
    }
  })
}

// 取消收藏文章接口模块
export const cancelCollectArticles = target => {
  return request({
    method: 'DELETE',
    url: `/app/v1_0/article/collections/${target}`
  })
}

// 对文章点赞
export const likeArticle = target => {
  return request({
    method: 'POST',
    url: '/app/v1_0/article/likings',
    data: {
      target
    }
  })
}

// 取消对文章点赞
export const cancelLikeArticle = target => {
  return request({
    method: 'DELETE',
    url: `/app/v1_0/article/likings/${target}`
  })
}
