// 搜索框联想记忆请求模块
import request from '@/utils/request'

// 获取联想关键词请求模块
export const getSearchSuggest = q => {
  return request({
    method: 'GET',
    url: '/app/v1_0/suggestion',
    params: {
      q
    }
  })
}

// 获取搜索结果请求模块
export const getSearchResults = params => {
  return request({
    method: 'GET',
    url: '/app/v1_0/search',
    params
  })
}
